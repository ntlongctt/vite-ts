import styled from '@emotion/styled';
import { ProductColor } from 'core/konva/ImageEditor/data/productState';
import { memo } from 'react';

type Props = {
  color: string;
  colorKey: ProductColor;
  checked?: boolean;
  onCheck: (c: ProductColor) => void;
};

const Container = styled.div({
  width: 20,
  height: 20,
  cursor: 'pointer',
});

const ColorCheck = ({ color, checked, onCheck, colorKey }: Props) => {
  return (
    <Container
      onClick={() => onCheck(colorKey)}
      style={{ backgroundColor: color, border: checked ? '2px solid' : 'none' }}
    />
  );
};

export default memo(ColorCheck);
