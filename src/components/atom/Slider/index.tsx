import { HTMLProps } from 'react';

const Slider = (props: HTMLProps<HTMLInputElement>) => {
  return <input {...props} type="range" />;
};

export default Slider;

