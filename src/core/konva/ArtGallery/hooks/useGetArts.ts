import { getArts } from 'apis/arts/getArts';
import { useQuery } from 'react-query';
import { APP_QUERY_KEYS } from 'shared/AppQueryKeys';

const useGetArts = () =>
  useQuery({
    queryKey: [APP_QUERY_KEYS.ART.GET_LIST],
    queryFn: ({ signal }) => getArts(),
    select(d) {
      return d.data;
    },
  });

export { useGetArts };

