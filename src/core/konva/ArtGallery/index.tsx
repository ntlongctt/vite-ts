import { useGetArts } from './hooks/useGetArts';

import { action as productActions } from '../ImageEditor/data/productState';
import { action as canvasAction } from '../ImageEditor/data/canvasState';

const ArtGallery = () => {
  const { data } = useGetArts();

  const handleSelectArg = (i: { id: string; url: string }) => {
    productActions.addFrontSvg(i);
    canvasAction.activeTool('');
  };

  if (!data) {
    return <></>;
  }
  return (
    <div style={{ display: 'flex', flexWrap: 'wrap', gap: 10 }}>
      {data.map((i) => (
        <div onClick={() => handleSelectArg(i)} style={{ cursor: 'pointer' }} key={i.id}>
          <img alt={`art-${i.id}`} src={i.url} width={80} />
        </div>
      ))}
    </div>
  );
};

export default ArtGallery;

