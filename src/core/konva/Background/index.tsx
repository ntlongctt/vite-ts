type Props = {
  width: number;
  height: number;
  color?: string;
  url: string;
};

const Background = ({ height, width, color, url }: Props) => {
  return (
    <img
      width={770}
      alt=""
      src={url}
    />
  );
};

export default Background;
