import create from 'zustand';
import { devtools } from 'zustand/middleware';

import { makeUtil } from 'utils/zustand';

import bananaUrl from '../../../../assets/svgs/banana.svg';

export type ProductColor = 'deepRed' | 'aquaticBlue' | 'white' | 'charcoalHeather';
export type ProductSize = 's' | 'm' | 'l';
export type ProductDirection = 'front' | 'back';

type CanvasObject = {
  id: string;
  x: number;
  y: number;
  w: number;
  h: number;
  url: string;
  type: ObjectType;
  rotate?: number;
  isDraft?: boolean;
  content?: string;
  fontSize?: number;
  textColor?: string;
  fontFamily?: string;
};

type ObjectType = 'text' | 'svg' | 'image';

type BodyPathConfig = {
  objects: CanvasObject[];
};

type ShirtConfig = {
  color: ProductColor;
  size: ProductSize;
};

interface IProduct {
  general: ShirtConfig;
  direction: Partial<Record<ProductDirection, BodyPathConfig>>;
}

type Action = {
  setProductColor: CallBack<ProductColor>;
  addFrontSvg: CallBack<{ id: string; url: string }>;
  rotateObject: CallBack<{ id: string; value: number }>;
  setTextContent: CallBack<{ id: string; value: string }>;
  setTextSize: CallBack<{ id: string; value: number }>;
  setTextColor: CallBack<{ id: string; value: string }>;
  setFontFamily: CallBack<{ id: string; value: string }>;
};

const initialState: IProduct = {
  general: {
    color: 'white',
    size: 'm',
  },
  direction: {
    front: {
      objects: [
        {
          id: '12-cd-12',
          x: 100,
          y: 140,
          w: 100,
          h: 100,
          type: 'svg',
          url: bananaUrl,
        },
        // {
        //   id: '123-cd-12',
        //   x: 250,
        //   y: 140,
        //   w: 200,
        //   h: 200,
        //   type: 'svg',
        //   url: 'https://www.svgrepo.com/show/428226/beach.svg',
        // },
      ],
    },
  },
};

const product = create<IProduct>()(devtools(() => initialState, { name: 'quill-canvas' }));

const productUtil = makeUtil(product.setState, product.getState);

const setProductColor = productUtil.updateState<Action['setProductColor']>((s, v) => {
  s.general.color = v;
});

const selectFrontObjectType = (t: ObjectType) => (s: IProduct) =>
  s.direction.front?.objects.filter((i) => i.type === t);

const selectProductColor = (s: IProduct) => s.general.color;

const addFrontSvg = productUtil.updateState<Action['addFrontSvg']>((s, v) => {
  s.direction.front?.objects.push({
    h: 100,
    w: 100,
    x: 100,
    y: 100,
    type: 'svg',
    id: v.id + Date.now() + '',
    url: v.url,
    isDraft: true,
  });
});

const addTextObject = productUtil.updateState((s) => {
  s.direction.front?.objects.push({
    type: 'text',
    h: 100,
    id: 'text ' + Date.now(),
    url: '',
    w: 100,
    x: 100,
    y: 100,
    content: 'text here',
    fontSize: 24,
    fontFamily: 'Arial',
  });
});

const findObject = (d: ProductDirection) => (id: string) => (s: IProduct) => {
  return s.direction[d]?.objects.find((i) => i.id === id);
};

const setTextSize = productUtil.updateState<Action['setTextSize']>((s, v) => {
  const _obj = s.direction.front?.objects.find((i) => i.id === v.id);
  if (!_obj) {
    return;
  }
  _obj.fontSize = v.value;
});

const setTextColor = productUtil.updateState<Action['setTextColor']>((s, v) => {
  const _obj = s.direction.front?.objects.find((i) => i.id === v.id);
  if (!_obj) {
    return;
  }
  _obj.textColor = v.value;
});

const setTextContent = productUtil.updateState<Action['setTextContent']>((s, v) => {
  const _obj = s.direction.front?.objects.find((i) => i.id === v.id);
  if (!_obj) {
    return;
  }
  _obj.content = v.value;
});

const setTextFontFamily = productUtil.updateState<Action['setFontFamily']>((s, v) => {
  const _obj = s.direction.front?.objects.find((i) => i.id === v.id);
  if (!_obj) {
    return;
  }
  _obj.fontFamily = v.value;
});

const rotateObject = productUtil.updateState<Action['rotateObject']>((s, v) => {
  // const _obj = findObject('front')(v.id)(s);
  const _obj = s.direction.front?.objects.find((i) => i.id === v.id);
  if (!_obj) {
    return;
  }
  _obj.rotate = v.value;
});

const action = {
  setProductColor,
  selectProductColor,
  addFrontSvg,
  selectFrontObjectType,
  findObject,
  rotateObject,
  addTextObject,
  setTextSize,
  setTextContent,
  setTextColor,
  setTextFontFamily,
};

export { product as productState, action };
