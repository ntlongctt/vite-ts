import { makeUtil } from 'utils/zustand';
import create from 'zustand';

import { ProductDirection } from './productState';

export const MENU = ['Products', 'Image', 'Text', 'Art'] as const;
export type ObjectType = 'image' | 'text' | 'art';

type State = {
  selectedObject:
    | {
        id: string;
        type: ObjectType;
      }
    | undefined;
  activatedBodyPath: ProductDirection;
  activatedTool: typeof MENU[number] | '';
};

type Action = {
  selectObject: CallBack<State['selectedObject']>;
  activeTool: CallBack<State['activatedTool']>;
};

const initialState: State = {
  selectedObject: undefined,
  activatedBodyPath: 'front',
  activatedTool: '',
};

const state = create<State>(() => initialState);
const stateUtil = makeUtil(state.setState, state.getState);

const getSelectedObject = (s: State) => s.selectedObject;

const selectObject = stateUtil.updateState<Action['selectObject']>((s, v) => {
  s.selectedObject = v;
});

const activeTool = stateUtil.updateState<Action['activeTool']>((s, v) => {
  s.activatedTool = v;
});

const getActivatedPath = (s: State) => s.activatedBodyPath;

const action = {
  get: {
    selectedObject: getSelectedObject,
    activatedPath: getActivatedPath,
  },
  selectObject,
  activeTool,
};

export { state, action };

