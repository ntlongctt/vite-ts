const BOUNDARY = {
  front: {
    adult: {
      x: 180,
      y: 150,
      w: 320,
      h: 450,
    },
    youth: {
      x: 210,
      y: 180,
      w: 260,
      h: 390,
    },
    leftChest: {
      x: 370,
      y: 170,
      w: 100,
      h: 100,
    },
  },
};

export { BOUNDARY };

