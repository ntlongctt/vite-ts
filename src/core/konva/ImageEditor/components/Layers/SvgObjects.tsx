import { Layer } from 'react-konva';
import { productState, action } from '../../data/productState';
import { state as canvasState } from '../../data/canvasState';
import ImageObject from '../ImageObject';

const SvgObjectsLayer = () => {
  const svgs = productState(action.selectFrontObjectType('svg'));
  const selectedObject = canvasState((s) => s.selectedObject);

  if (!svgs?.length) {
    return <></>;
  }

  return (
    <>
      {svgs.map((i) => (
        <ImageObject key={i.id} id={i.id} url={i.url} selected={selectedObject?.id === i.id} />
      ))}
    </>
  );
};

export default SvgObjectsLayer;
