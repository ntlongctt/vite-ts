import { Rect, Text } from 'react-konva';

import { BOUNDARY } from '../../constants/bodyBoundary';

const { adult, leftChest, youth } = BOUNDARY.front;

const Adult = ({ visible }: { visible: boolean }) => {
  return (
    <>
      <Text y={adult.y + 10} x={adult.x + 10} visible={visible} fill="grey" text="Adult" />
      <Rect
        strokeEnabled={visible}
        x={adult.x}
        y={adult.y}
        width={adult.w}
        height={adult.h}
        stroke="grey"
      />
    </>
  );
};

const Youth = ({ visible }: { visible: boolean }) => {
  return (
    <>
      <Text visible={visible} fill="grey" x={youth.x + 10} y={youth.y + 10} text="Youth" />
      <Rect
        x={youth.x}
        y={youth.y}
        strokeEnabled={visible}
        width={youth.w}
        height={youth.h}
        stroke="grey"
      />
    </>
  );
};

const LeftChest = ({ visible }: { visible: boolean }) => {
  return (
    <>
      <Text
        visible={visible}
        fill="grey"
        x={leftChest.x + 5}
        y={leftChest.y + 5}
        text="Left Chest"
      />
      <Rect
        x={leftChest.x}
        y={leftChest.y}
        strokeEnabled={visible}
        width={leftChest.w}
        height={leftChest.h}
        stroke="grey"
      />
    </>
  );
};

const FrontBoundary = ({ visible }: { visible: boolean }) => {
  return (
    <>
      <Adult visible={visible} />
      <Youth visible={visible} />
      <LeftChest visible={visible} />
    </>
  );
};

export default FrontBoundary;

