import { Layer } from 'react-konva';

import { action, state } from '../../data/canvasState';
import FrontBoundary from './FrontBoundary';

const BoundLayer = ({ visible }: { visible: boolean }) => {
  const activatedPath = state(action.get.activatedPath);
  return <Layer>{activatedPath === 'front' ? <FrontBoundary visible={visible} /> : null}</Layer>;
};

export default BoundLayer;

