import { productState, action } from '../../data/productState';
import TextObject from '../TextObject';

const TextLayer = () => {
  const textObjects = productState(action.selectFrontObjectType('text'));
  console.log({ textObjects });

  return (
    <>
      {textObjects?.map((t) => (
        <TextObject
          fill={t.textColor || 'red'}
          fontSize={t.fontSize}
          text={t.content}
          id={t.id}
          draggable
          x={t.x}
          y={t.y}
          key={t.id}
          fontFamily={t.fontFamily || 'Arial'}
        />
      ))}
    </>
  );
};

export default TextLayer;
