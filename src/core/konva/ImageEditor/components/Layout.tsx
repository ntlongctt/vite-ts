import styled from '@emotion/styled';
import Background from 'core/konva/Background';
import { theme } from 'theme/theme';
// import Main from './Main';
import Main from './Main_2';
import SelectedObject from './SelectedObject';
import ToolBox from './Toolbox';

const Layout = () => {
  return (
    <Container>
      <ToolBox></ToolBox>
      <SelectedObject />
      <RightSide>
        <Heading>heading</Heading>
        <MainContainer>
          <Main />
          {/* <Background height={400} width={300} /> */}
        </MainContainer>
      </RightSide>
    </Container>
  );
};

export default Layout;

const MainContainer = styled.div({
  position: 'relative',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
});

const Container = styled.div({
  display: 'flex',
  gap: 10,
  border: '1px dashed',
});

const RightSide = styled.div({
  flex: 1,
  border: '1px dashed red',
});

const Heading = styled.div({
  background: 'rebeccapurple',
});
