import Konva from 'konva';
import { useEffect, useRef } from 'react';

import { state as canvasStage } from '../data/productState';

const config = {
  width: 300,
  height: 400,
};

const Main = () => {
  const state = useRef<Konva.Stage>();

  useEffect(() => {
    if (!state.current) {
      state.current = new Konva.Stage({
        container: 'container',
        width: config.width,
        height: config.height,
      });

      const layer = new Konva.Layer();

      const boundRec = new Konva.Rect({
        x: 0,
        y: 0,
        width: config.width,
        height: config.height,
        stroke: 'red',
        strokeWidth: 2,
      });

      const bound_2 = boundRec.clone({
        x: 15,
        y: 15,
        width: config.width - 30,
        height: config.height - 30,
        stroke: 'blue',
        strokeWidth: 2,
      });

      const box = new Konva.Rect({
        x: 50,
        y: 50,
        fill: 'yellow',
        stroke: 'black',
        strokeWidth: 4,
        draggable: true,
        width: 100,
        height: 50,
      });

      layer.add(boundRec);
      layer.add(bound_2);
      layer.add(box);
      state.current.add(layer);
    }
  }, []);

  return <div style={{ position: 'absolute' }} id="container"></div>;
};

export default Main;
