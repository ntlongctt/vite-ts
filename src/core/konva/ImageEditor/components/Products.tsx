import styled from '@emotion/styled';
import ColorCheck from 'components/atom/ColorCheck';
import { useCallback } from 'react';

import { action, productState, ProductColor } from '../data/productState';

export const COLOR = {
  deepRed: 'rgb(195, 21, 58)',
  aquaticBlue: 'rgb(123, 191, 236)',
  white: '#fff',
  charcoalHeather: 'rgb(74, 75, 77)',
} as Record<ProductColor, string>;

const Colors = styled.div({
  display: 'flex',
  flexWrap: 'wrap',
  gap: 4,
});

const Products = () => {
  const selectedColor = productState(action.selectProductColor);

  const handleCheck = useCallback((c: ProductColor) => {
    action.setProductColor(c);
  }, []);

  return (
    <div style={{ marginBottom: 20 }}>
      <p>choose color:</p>
      <Colors>
        {Object.entries(COLOR).map(([k, v]) => (
          <ColorCheck
            colorKey={k as ProductColor}
            checked={selectedColor === k}
            onCheck={handleCheck}
            key={k}
            color={v}
          />
        ))}
      </Colors>
    </div>
  );
};

export default Products;
