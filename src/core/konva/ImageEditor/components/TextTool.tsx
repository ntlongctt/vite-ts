import { Button } from '@mantine/core';

import { action } from '../data/productState';

const TextConfig = () => {
  return (
    <div>
      <Button color="orange" onClick={action.addTextObject}>
        Add new text
      </Button>
    </div>
  );
};

export default TextConfig;
