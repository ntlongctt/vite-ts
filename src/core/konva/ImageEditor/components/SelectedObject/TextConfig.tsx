import { Avatar, ColorPicker, DEFAULT_THEME, Group, Input, Select, Slider } from '@mantine/core';
import { forwardRef, useCallback } from 'react';
import { state as canvasState } from '../../data/canvasState';
import { productState, action as productActions } from '../../data/productState';

const TextConfig = () => {
  const _selected = canvasState((s) => s.selectedObject);

  const _sObj = productState(productActions.findObject('front')(_selected?.id || ''));

  if (!_sObj || !_selected) {
    return <></>;
  }
  console.log('new', _sObj.id);

  const handleChangeSize = (v: number) => {
    const id = canvasState.getState().selectedObject?.id;
    if (!id) {
      return;
    }
    productActions.setTextSize({ id: id, value: v });
  };

  return (
    <div>
      <p>Content:</p>
      <Input
        value={_sObj.content}
        onChange={(e) => productActions.setTextContent({ id: _sObj.id, value: e.target.value })}
      />
      <p>Size: {_sObj.fontSize}</p>
      <Slider onChange={(v) => handleChangeSize(v)} min={1} max={200} value={_sObj.fontSize} />

      <p>color</p>
      <ColorPicker
        format="hex"
        value={_sObj.textColor || DEFAULT_THEME.colors.dark[9]}
        onChange={(v) => productActions.setTextColor({ id: _sObj.id, value: v })}
        withPicker={false}
        fullWidth
        swatches={[
          DEFAULT_THEME.colors.red[9],
          DEFAULT_THEME.colors.green[9],
          DEFAULT_THEME.colors.blue[9],
          DEFAULT_THEME.colors.cyan[9],
          DEFAULT_THEME.colors.grape[9],
          DEFAULT_THEME.colors.orange[9],
          DEFAULT_THEME.colors.violet[9],
          DEFAULT_THEME.colors.yellow[3],
        ]}
      />

      <p>Font Family</p>

      <Select
        placeholder="Pick one"
        value={_sObj.fontFamily}
        onChange={(v) => productActions.setTextFontFamily({ id: _sObj.id, value: v || 'Arial' })}
        data={[
          { value: '"Climate Crisis", cursive', label: 'Climate Crisis' },
          { value: "'Tilt Prism', cursive", label: 'Tilt Prism' },
          { value: "'Mynerve', cursive", label: 'Mynerve' },
          { value: "'Rubik Iso', cursive", label: 'Rubik Iso' },
          { value: 'Arial', label: 'Arial' },
        ]}
        itemComponent={SelectItem}
      />
    </div>
  );
};

export default TextConfig;

interface ItemProps extends React.ComponentPropsWithoutRef<'div'> {
  label: string;
  value: string;
}

// eslint-disable-next-line react/display-name
const SelectItem = forwardRef<HTMLDivElement, ItemProps>(
  ({ label, value, ...others }: ItemProps, ref) => (
    <div style={{ fontFamily: value }} ref={ref} {...others}>
      {label}
    </div>
  )
);
