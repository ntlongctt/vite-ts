import { state, ObjectType } from '../../data/canvasState';
import TextConfig from './TextConfig';
import ArtConfig from './ArtConfig';

const MAPPING = {
  art: <ArtConfig />,
  image: <div>img</div>,
  text: <TextConfig />,
} as Record<ObjectType, JSX.Element>;

const SelectedObject = () => {
  const selectedObject = state((s) => s.selectedObject);

  if (!selectedObject) {
    return null;
  }

  return (
    <div>
      <p>
        Type: {selectedObject.type}, Id: {selectedObject.id}
      </p>
      <div>{MAPPING[selectedObject.type]}</div>
    </div>
  );
};

export default SelectedObject;
