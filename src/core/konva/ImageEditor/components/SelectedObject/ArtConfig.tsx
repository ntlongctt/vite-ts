import { Slider } from '@mantine/core';
import { useCallback, useEffect, useRef } from 'react';
import { state as canvasState } from '../../data/canvasState';
import { productState, action as productActions } from '../../data/productState';

const ArtConfig = () => {
  const _selected = canvasState((s) => s.selectedObject);

  const _sObj = productState(productActions.findObject('front')(_selected?.id || ''));

  const handleRotate = useCallback((v: number) => {
    const _s = canvasState.getState().selectedObject?.id;
    if (!_s) {
      return;
    }

    productActions.rotateObject({
      id: _s,
      value: v,
    });
  }, []);

  useEffect(() => {
    console.log(_sObj?.id);
    if (!_sObj) {
      return;
    }
  }, [_sObj]);

  console.log(_sObj?.rotate, _sObj?.id);

  return (
    <div>
      <p>Rotation:</p>
      <Slider value={_sObj?.rotate} onChange={handleRotate} min={-180} max={180} />
      <p>change size</p>
      <div>
        {/* <input value={_sObj?.w} placeholder="width" />
        <input value={_sObj?.h} placeholder="height" /> */}
        <button>update</button>
      </div>
    </div>
  );
};
export default ArtConfig;
