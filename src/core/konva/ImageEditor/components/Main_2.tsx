import { useImage } from 'hooks/useImage';
import usePrevious from 'hooks/usePrevious';
import React, { RefObject, useEffect, useReducer, useRef, useState } from 'react';
import { Image, Layer, Rect, Stage, Text, StageProps } from 'react-konva';
import type Konva from 'konva';

import { action, ProductColor, productState } from '../data/productState';
import ImageObject from './ImageObject';
import BoundLayer from './Layers/PathBoundary';
import SvgObjectsLayer from './Layers/SvgObjects';
import TextObject from './TextObject';
import TextLayer from './Layers/TextLayer';

const imageRatio = 944 / 805;

const config = {
  w: 700,
  h: 700 * imageRatio,
};

const adult = {
  x: 180,
  y: 150,
  w: 320,
  h: 450,
};

const IMG = {
  deepRed:
    'https://mms-images-prod.imgix.net/mms/images/catalog/3dd0232d15912147c7e66ef342515c6f/colors/116204/views/alt/front_large_extended.png?dpr=1.2&auto=format&nrs=0&w=1000',
  aquaticBlue:
    'https://mms-images-prod.imgix.net/mms/images/catalog/4df5d070123dd52e696e883ce617e555/colors/116242/views/alt/front_large_extended.png?dpr=1.2&auto=format&nrs=0&w=1000',
  white:
    'https://mms-images-prod.imgix.net/mms/images/catalog/57494ecaa0e6f8f3dd6e3966d67e2917/colors/116200/views/alt/front_large_extended.png?dpr=1.2&auto=format&nrs=0&w=1000',
  charcoalHeather:
    'https://mms-images-prod.imgix.net/mms/images/catalog/edcedde9c540ad049fe464d5b14b56b9/colors/116244/views/alt/front_large_extended.png?dpr=1.2&auto=format&nrs=0&w=1000',
} as Record<ProductColor, string>;

type Action = {
  onDragMove: StageProps['onDragMove'];
};

const Main = () => {
  const productColor = productState(action.selectProductColor);
  const [isDragging, toggleDragging] = useReducer((s) => !s, false);
  const [image, loading] = useImage(IMG[productColor], 'anonymous');
  const prevImg = usePrevious(image);

  const ref = useRef<Konva.Stage>(null);
  const printRef = useRef<Konva.Layer>(null);

  const downloadURI = (uri: string, name: string) => {
    const link = document.createElement('a');
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleSavePrint = (event: any) => {
    event.preventDefault();
    const dataURL = printRef.current?.toDataURL({
      mimeType: 'image/jpeg',
      quality: 0,
      pixelRatio: 16,
    });
    downloadURI(dataURL || '', 'printAble');
  };

  const handleSaveImage = (event: any) => {
    event.preventDefault();
    const dataURL = ref.current?.toDataURL({
      mimeType: 'image/jpeg',
      quality: 0,
      pixelRatio: 16,
    });
    downloadURI(dataURL || '', 'preview');
  };

  return (
    <div>
      <button onClick={handleSaveImage}>save</button>
      <button onClick={handleSavePrint}>save print</button>
      <Stage
        onDragEnd={toggleDragging}
        onDragStart={toggleDragging}
        onDragMove={handleItemMove}
        width={config.w}
        height={config.h}
        ref={ref}
      >
        <Layer>
          <Image width={config.w} height={config.h} x={0} y={0} image={image || prevImg} />
        </Layer>
        <BoundLayer visible={isDragging} />
        <Layer ref={printRef}>
          <SvgObjectsLayer />
          <TextLayer />
        </Layer>
      </Stage>
    </div>
  );
};

// const SvgObjectsLayer = () => {
//   const svgs = productState((s) => s.direction.front?.svg);
//   console.log('SvgObjectsLayer ');

//   if (!svgs?.length) {
//     return <></>;
//   }
//   console.log({ svgs });

//   return (
//     <Layer>
//       <ImageObject
//         id="12"
//         url="https://www.svgrepo.com/show/429765/banana-blackbarry-blackberries.svg"
//       />
//     </Layer>
//   );
// };

const handleItemMove: Action['onDragMove'] = (e) => {
  const { target } = e;
  const { height, width } = target.getSize();
  const newPos = { ...target.getAbsolutePosition() };
  if (newPos.x < adult.x) {
    newPos.x = adult.x;
  }
  if (newPos.x + width > adult.x + adult.w) {
    newPos.x = adult.x + adult.w - width;
  }

  if (newPos.y < adult.y) {
    newPos.y = adult.y;
  }
  if (newPos.y + height > adult.y + adult.h) {
    newPos.y = adult.y + adult.h - height;
  }

  target.setAbsolutePosition(newPos);
};

export default Main;
