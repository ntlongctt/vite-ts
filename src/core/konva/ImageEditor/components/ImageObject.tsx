import { Image } from 'react-konva';

import { useImage } from 'hooks/useImage';
import { action } from '../data/canvasState';
import { productState, action as productAction } from '../data/productState';
import { memo, useCallback } from 'react';

type Props = {
  id: string;
  url: string;
  selected: boolean;
};

const ImageObject = ({ id, selected, url }: Props) => {
  const [img] = useImage(url, 'anonymous');

  const selectedObject = productState(productAction.findObject('front')(id));
  console.log(selectedObject?.url, url);

  const handleClick = useCallback(() => {
    console.log('click');
    action.selectObject({
      id,
      type: 'art',
    });
  }, []);

  return (
    <Image
      rotation={selectedObject?.rotate}
      offset={{
        x: 40,
        y: 40,
      }}
      x={100}
      y={100}
      height={80}
      width={80}
      strokeEnabled={selected}
      dash={[10, 10]}
      stroke="#ff9800"
      onClick={handleClick}
      draggable
      image={img}
    />
  );
};

export default memo(ImageObject);
