import { ComponentProps } from 'react';
import { Text } from 'react-konva';

import { action } from '../data/canvasState';

type Props = ComponentProps<typeof Text> & {
  id: string;
};

const TextObject = ({ id, ...rawProps }: Props) => {
  const handleClick = () => {
    console.log(id);
    action.selectObject({ id, type: 'text' });
  };

  return <Text onClick={handleClick} {...rawProps}></Text>;
};

export default TextObject;
