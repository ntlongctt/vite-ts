import styled from '@emotion/styled';
import { Box, Button, NavLink } from '@mantine/core';
import ArtGallery from 'core/konva/ArtGallery';
import { MENU, action, state } from '../data/canvasState';

import Products from './Products';
import TextConfig from './TextTool';

type Menu = typeof MENU[number];

const TOOL_MAPPING = {
  Products: <Products />,
  Text: <TextConfig />,
  Image: <div>img</div>,
  Art: <ArtGallery />,
} as Record<Menu, JSX.Element>;

const ToolBox = () => {
  // const [activeTool, setActiveTool] = useState<Menu | ''>('');
  const activeTool = state((s) => s.activatedTool);

  return (
    <Container>
      <Menu>
        <Box>
          {MENU.map((i) => (
            <NavLink onClick={() => action.activeTool(i)} key={i} label={i} />
          ))}
        </Box>
      </Menu>
      {activeTool && (
        <Tool>
          {TOOL_MAPPING[activeTool]}
          <Button onClick={() => action.activeTool('')}>close</Button>
        </Tool>
      )}
    </Container>
  );
};

export default ToolBox;

const Container = styled.div({
  display: 'flex',
});

const Tool = styled.div({
  width: 200,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
});

const Menu = styled.div({});
