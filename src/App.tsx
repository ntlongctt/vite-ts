import { useState, useEffect } from 'react';
import { Button, Global } from '@mantine/core';
import styled from '@emotion/styled';

import { theme } from 'theme/theme';

import './theme/variable.css';

const PrimaryText = styled.div({
  padding: 20,
  color: theme.colors.primary,
  backgroundColor: theme.colors.background,
});

function ThemeToggler() {
  const [theme, setTheme] = useState('light');
  const nextTheme = theme === 'light' ? 'dark' : 'light';
  useEffect(() => {
    document.body.dataset.theme = theme;
  }, [theme]);
  return <button onClick={() => setTheme(nextTheme)}>Change to {nextTheme} mode</button>;
}
const GlobalStyle = () => {
  return (
    <Global
      styles={() => ({
        '.mantine-Modal-modal': {
          padding: 0,
        },
        '.mantine-Modal-header': {
          margin: 0,
          color: 'white',
          height: 54,
          padding: '16px 10px 16px 20px',
          backgroundColor: '#2F80ED;',
        },
        '.mantine-Modal-close': {
          color: 'white',
          '&:hover': {
            backgroundColor: 'transparent',
          },
          svg: {
            width: 24,
            height: 24,
          },
        },
        '.mantine-Modal-body': {
          padding: 20,
        },
      })}
    />
  );
};

function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <header className="App-header">
        <Button color="blue">Click Me</Button>
        <p className="my-class">test</p>
      </header>
      <PrimaryText>test text</PrimaryText>
      <ThemeToggler />
    </div>
  );
}

export default App;
