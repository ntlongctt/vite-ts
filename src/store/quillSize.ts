import create from 'zustand';
import { devtools } from 'zustand/middleware';

type QuillType = 'Crib' | 'Twin/Single' | 'Full/Double' | 'King';

type Layout = {
  col: number;
  row: number;
};

type State = {
  type: QuillType;
  layout: Layout;
};

const initialState: State = {
  layout: {
    col: 4,
    row: 4,
  },
  type: 'Crib',
};

const state = create(
  devtools(() => initialState, {
    name: 'Quill_Setting',
  })
);

const setQuillConfig = (config: State) => state.setState(() => config);

const action = {
  setQuillConfig,
};

export { state, action };
