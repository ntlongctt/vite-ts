import { Outlet, Link } from 'react-router-dom';

import { QueryClientProvider, QueryClient } from 'react-query';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

const ProvidersWrapper = ({ children }: { children: JSX.Element }) => {
  return (
    <>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
    </>
  );
};

function Root() {
  return (
    <>
      <div id="sidebar">
        <Link to="config-quill">build image</Link>
      </div>
      <div id="detail">
        <Outlet />
      </div>
    </>
  );
}

const AppRoot = () => {
  return (
    <ProvidersWrapper>
      <Root />
    </ProvidersWrapper>
  );
};

export default AppRoot;
