declare type VoidCallback = () => void;

declare type CallBack<T> = (_v: T) => void;

declare type TCallBack<K, T> = (_v: K) => T;

