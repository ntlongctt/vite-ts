import { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';

import Root from './routes/root';

import './index.css';

import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import ErrorPage from 'pages/status/ErrorPage';
import ImageEditor from 'pages/private/ImageEditor';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: 'config-quill',
        element: <ImageEditor />,
      },
    ],
  },
]);

const container = document.getElementById('root');
if (container) {
  const root = ReactDOM.createRoot(container);
  root.render(
    <StrictMode>
      <RouterProvider router={router} />
    </StrictMode>
  );
}
