type Response<T> = {
  status: 'ok' | 'error';
  data: T;
};

type Art = {
  id: string;
  url: string;
};

type ApiResponse = Response<Art[]>;

const getArts = (): Promise<ApiResponse> => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res({
        status: 'ok',
        data: DATA,
      });
    }, 500);
  });
};

export { getArts };

const DATA = [
  {
    id: 'abc',
    url: 'https://www.svgrepo.com/show/429066/animal-australia-bear.svg',
  },
  {
    id: '1212',
    url: 'https://www.svgrepo.com/show/428226/beach.svg',
  },
  {
    id: '1291',
    url: 'https://www.svgrepo.com/show/416541/animal-cartoon-cow.svg',
  },
  {
    id: '111',
    url: 'https://www.svgrepo.com/show/429075/aquarium-dolphin-fish.svg',
  },
];

