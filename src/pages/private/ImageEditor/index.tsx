import Editor from 'core/konva/ImageEditor';

const ImageEditor = () => {
  return (
    <div>
      <Editor />
    </div>
  );
};

export default ImageEditor;
