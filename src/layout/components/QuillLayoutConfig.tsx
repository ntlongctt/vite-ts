import { Text, Group, createStyles } from '@mantine/core';

const styles = createStyles(() => ({
  sizeBlock: {
    border: '1px solid',
  },
}));

const QuillSizeSelect = () => {
  console.log('QuillSizeSelect  ');
  const { classes } = styles();
  return (
    <Group grow>
      <div className={classes.sizeBlock}>
        <Text>Crib</Text>
        <Text>W:28 x H:52</Text>
      </div>
      <div className={classes.sizeBlock}>
        <Text>Twin/Single</Text>
        <Text>W:28 x H:52</Text>
      </div>
      <div className={classes.sizeBlock}>
        <Text>Full/Double</Text>
        <Text>W:28 x H:52</Text>
      </div>
      <div className={classes.sizeBlock}>
        <Text>King</Text>
        <Text>W:28 x H:52</Text>
      </div>
    </Group>
  );
};

const QuillLayoutConfig = () => {
  console.log('QuillLayoutConfig ');
  return (
    <div>
      <Text weight={700}>Select Quilt Size (inch)</Text>
      <QuillSizeSelect />
      <Text weight={700}>Select Quilt Layout</Text>
    </div>
  );
};

export default QuillLayoutConfig;
